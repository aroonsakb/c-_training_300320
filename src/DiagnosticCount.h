#pragma once
#ifndef __DIAGNOSTICCOUNT_H__
#define __DIAGNOSTICCOUNT_H__

#include <iostream>
template <typename T>
class DiagnosticCount
{
    static int _count;
    int _index;

protected:
    DiagnosticCount() : _index(++_count)
    {
        std::cout << "Creating object no: " << _index << std::endl;
    }
    virtual ~DiagnosticCount()
    {
        std::cout << "Destroying object no: " << _index << std::endl;
    }
};

template <typename T>
int DiagnosticCount<T>::_count = 0;

// template <typename T>
// int DiagnosticCount
#endif
// __DIAGNOSTICCOUNT_H__