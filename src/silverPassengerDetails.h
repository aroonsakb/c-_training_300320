#pragma once

#ifndef __SILVERPASSENGERDETAILS_H__
#define __SILVERPASSENGERDETAILS_H__
#include <iostream>
#include "Flight.h"

namespace flighthandling
{
class SilverPassengerDetails : public PassengerDetails
{
public:
    SilverPassengerDetails(const std::string &name, const int &weight) : PassengerDetails(name, weight)
    {
    }
    SilverPassengerDetails() : PassengerDetails() {}
    void input() override
    {
        std::cout << "silver club passenger:" << std::endl;
        PassengerDetails::input();
    }
    void output() const override
    {
        std::cout << "silver club passenger:" << std::endl;
        PassengerDetails::output();
    }
};
} // namespace flighthandling

#endif
// __SILVERPASSENGERDETAILS_H__