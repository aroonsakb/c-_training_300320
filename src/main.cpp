#include <iostream>
#include <string>
#include "Flight.h"
#include "silverPassengerDetails.h"

// int DiagnosticCount<int>::_count = 0;
// class AClass : DiagnosticCount<int>
// {
// };

int main()
{
	// int MAXIMUMPASSENGER = 4;
	using flighthandling::PassengerDetails;
	using flighthandling::SilverPassengerDetails;

	// AClass ac1, ac2;
	bool exit = false;
	std::string name;
	int weight = 0;
	char options = '\0';
	const int MAXIMUMPASSENGER = 4;
	std::shared_ptr<PassengerDetails> pd = nullptr;
	std::shared_ptr<PassengerDetails> details[MAXIMUMPASSENGER];
	unsigned int count = 0;
	do
	{
		std::cout << "Please enter an option : \n"
				  << "i = insert\n"
				  << "s = insert silver passenger\n"
				  << "l  = list\n"
				  << "x = exit\n>";
		std::cin >> options;
		try
		{
			switch (std::tolower(options))
			{
			case 'i':

				pd = std::shared_ptr<PassengerDetails>(new PassengerDetails());
				pd->input();
				details[count] = pd;
				++count;

				break;
			case 's':
				pd = std::shared_ptr<PassengerDetails>(new SilverPassengerDetails());
				pd->input();
				details[count] = pd;
				++count;

				break;
			case 'l':
				for (size_t i = 0; i < count; i++)
				{
					details[i]->output();
				}
				break;
			case 'x':
				exit = true;
			}
		}
		catch (const std::bad_alloc &ba)
		{
			std::cout << "Out of memory!" << ba.what() << std::endl;
		}
		catch (const std::exception &ex)
		{
			std::cout << "General Problems" << ex.what() << std::endl;
		}
	} while (exit != true);
	// for (size_t i = 0; i < count; i++)
	// {
	// 	delete details[i];
	// }
}