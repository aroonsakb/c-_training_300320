#pragma once
#ifndef __FLIGHT_H__
#define __FLIGHT_H__
#include "DiagnosticCount.h"

namespace flighthandling
{
class PassengerDetails : private DiagnosticCount<PassengerDetails>
{
private:
    std::string _name;
    int _weight;

public:
    PassengerDetails() : PassengerDetails("Unknown", 0) {}
    virtual ~PassengerDetails() noexcept {}
    PassengerDetails(const std::string &name, const int &weight) : _name(name), _weight(weight) {}
    std::string getName() const { return _name; }
    void setName(const std::string &name) { _name = name; }
    int getWeight() { return _weight; }

    virtual void input()
    {
        std::cout << "Please enter a name:";
        std::cin >> this->_name;
        std::cout << "Enter the baggage weight :";
        std::cin >> this->_weight;
    };
    virtual void output() const
    {
        printf("Name : %s , weight : %i \n", this->_name.c_str(), this->_weight);
    };
};
} // namespace flighthandling

#endif
// __FLIGHT_H__